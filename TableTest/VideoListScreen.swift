//
//  VideoListScreen.swift
//  TableTest
//
//  Created by Carlos Ruiz on 7/9/18.
//  Copyright © 2018 Carlos Ruiz. All rights reserved.
//

import UIKit
import MediastreamPlatformSDK

class VideoListScreen: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var videos: [Video] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videos = createArray()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func createArray() -> [Video] {
        var tempVideos: [Video] = []
        let video1 = Video(image: #imageLiteral(resourceName: "hellsing"), title: "VOD", id: "5b5269c08e89904d9ee7f99b", type: MediastreamPlayerConfig.VideoTypes.VOD)
        let video2 = Video(image: #imageLiteral(resourceName: "hellsing"), title: "VOD", id: "5b649c74df366e076743b66c", type: MediastreamPlayerConfig.VideoTypes.VOD)
        let video3 = Video(image: #imageLiteral(resourceName: "ghoul"), title: "LIVE", id: "57b4db886338448314449cfa", type: MediastreamPlayerConfig.VideoTypes.LIVE)
        
        tempVideos.append(video1)
        tempVideos.append(video2)
        tempVideos.append(video3)
        
        return tempVideos
    }
}

extension VideoListScreen: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        
        cell.setVideo(video: video)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = videos[indexPath.row]
        let vc = storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController
        vc?.config.accountID = "579bd29dc99290cf08362c3b"
        vc?.config.type = video.type
        vc?.config.id = video.id
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
