//
//  Video.swift
//  TableTest
//
//  Created by Carlos Ruiz on 7/10/18.
//  Copyright © 2018 Carlos Ruiz. All rights reserved.
//

import Foundation
import UIKit
import MediastreamPlatformSDK

class Video {
    var image: UIImage
    var title: String
    var id: String
    var type: MediastreamPlayerConfig.VideoTypes
    
    init(image: UIImage, title: String, id: String, type: MediastreamPlayerConfig.VideoTypes) {
        self.image = image
        self.title = title
        self.id = id
        self.type = type
    }
}
