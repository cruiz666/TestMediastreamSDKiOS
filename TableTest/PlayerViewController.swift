//
//  PlayerViewController.swift
//  TableTest
//
//  Created by Carlos Ruiz on 7/10/18.
//  Copyright © 2018 Carlos Ruiz. All rights reserved.
//

import UIKit
import MediastreamPlatformSDK

class PlayerViewController: UIViewController {
    
    let config = MediastreamPlayerConfig()
    let mdstrm = MediastreamPlatformSDK()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addChild(mdstrm)
        self.view.addSubview(mdstrm.view)
        mdstrm.setup(config)
        mdstrm.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        mdstrm.releasePlayer()
    }
}
