//
//  ViewCell.swift
//  TableTest
//
//  Created by Carlos Ruiz on 7/10/18.
//  Copyright © 2018 Carlos Ruiz. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    
    func setVideo(video: Video) {
        videoImageView.image = video.image
        videoTitleLabel.text = video.title
    }
}
